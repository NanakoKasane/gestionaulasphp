<p><b>Proyecto para la gestión de aulas en un instituto.</b> Este proyecto principalmente permite buscar las aulas del instituto y reservarlas (solo si el aula estuviera libre para ese día y tramo lectivo) </p>
<p>El proyecto usa <b><i>PHP</b></i> con bootstrap y <b><i>MySQL</b></i></p>
<br/>
<ul>
    <li><p>Se puede loguear en la aplicación para probarla con el siguiente enlace: <i>http://89.39.159.65/aulas/gestionAula/php/login.php</i> y posteriormente se podrá buscar un aula y reservarla</p></li>
    <li><p>Guía sobre lo que se ha implementado en cada parte: <i>http://89.39.159.65/aulas/gestionAula.pdf</i> </p></li>
</ul>
<br/>
