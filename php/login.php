<?php
include ("App.php");

App::print_head("Inicio de sesión");
// App::print_nav1();

?>

<div class="container" >
    <div class="row">
        <div class="col-12 col-md-4 offset-md-4">
            <form method='POST' action='./login.php'>
                <br/><label for='user'>Nombre de usuario: </label> <br/> <input class="form-control"  type='text' name='username' id='user' required/><br/><br/>
                <label>Contraseña: </label><br/> <input class="form-control" type='password' name='password' required/><br/><br/>
                <input class="bg-primary form-control text-white" type="submit" value="Iniciar sesión"/><br/><br/>
                <a class="text-center text-white bg-primary form-control" href='./register.php' >Registro</a>
            </form>
        </div>
    </div> <!-- row -->
</div> <!-- Container -->


<?php
include_once("App.php");
App::print_footer();

// Compruebo que el usuario existe en la BBDD
if ($_POST){
    if (isset($_POST["username"]) && isset($_POST["password"])){
        $app = new App();
        $username = $_POST["username"];
        $password = $_POST["password"];
        $result = $app -> validateUser($username, $password);

        if ($result){
            // echo "Inicio de sesión correcto";
            $app = new App();
            $app -> saveSession($username);    
            echo "<script language='javascript'>window.location.href='./busquedaaula.php'</script>";

        }
        else{
            echo "<script language='javascript'>window.alert('El usuario no está registrado') </script>";
        }

    }

}

?>