<?php
    define ("DATABASE", "aula2");
    define ("DSN", "mysql:host=localhost;dbname=" . DATABASE);
    define ("USER", "www-data");
    define ("PASSWORD", "www-data");

    // Tablas
    define ("TABLE_USER", "user");
    define ("TABLE_AULA", "aula");
    define ("TABLE_TRAMO", "tramo");
    define ("TABLE_RESERVA", "reserva");
    define ("TABLE_USOSAULA", "usosaula");
    define ("TABLE_RESERVASANULADAS", "reservasAnuladas");

    // Columnas
    define ("COLUMN_USERNAME", "username");
    define ("COLUMN_PASSWORD", "password");
    define ("COLUMN_EMAIL", "email");
    define ("COLUMN_SHORTNAME", "shortname");
    define ("COLUMN_NAME", "name");
    define ("COLUMN_FECHA", "fecha");
    define ("COLUMN_AULA", "aula");
    define ("COLUMN_TRAMO", "tramo");
    define ("COLUMN_ID_RESERVADAS", "id");
    define ("COLUMN_USER_CATEGORY", "category");


class DAO{

    // Variables
    public $conn;

    function __construct(){
        try{
            $this -> conn = new PDO(DSN, USER, PASSWORD);
        }
        catch(PDOException $e){
            echo $e -> getMessage();
        }
    }


    // Devuelve true si el usuario con esa contraseña está registrado en la base de datos. Si el usuario no está en la BBDD, devuelve false.
    function validateUser($username, $passowrd){
        $sql = "select count(*) from " . TABLE_USER . " where " . COLUMN_USERNAME . " = ? and " . COLUMN_PASSWORD . " = sha1(?)";
        $statement = $this -> conn -> prepare($sql);
        // echo $sql;
        
        $statement -> execute(array($username, $passowrd));

        $columns = $statement -> fetchAll();
        if ($columns[0]["count(*)"] == 1)
            return true;
        else
            return false;
    }


    // Devuelve true si el usuario existe ya en la BBDD. Si no existe, false
    function getIfUserExists($username){
        $sql = "select count(*) from " . TABLE_USER . " where " . COLUMN_USERNAME . " = ?";
        $statement = $this -> conn -> prepare($sql);
        $statement -> execute(array($username));

        $columns = $statement -> fetchAll();
        if ($columns[0]["count(*)"] == 1)
            return true;
        else
            return false;

    }

    // Devuelve true si el email existe ya en la BBDD. Si no existe, false
    function getIfEmailExists($email){
        $sql = "select count(*) from " . TABLE_USER . " where " . COLUMN_EMAIL . " = ?";
        $statement = $this -> conn -> prepare($sql);
        $statement -> execute(array($email));

        $columns = $statement -> fetchAll();
        if ($columns[0]["count(*)"] == 1)
            return true;
        else
            return false;

    }

    // Registra el usuario
    function registerUser($username, $password, $fullname, $birthdate, $email){
        $sql = "insert into " . TABLE_USER . " values(?, sha1(?), ?, ?, ?, ?)";
        $statement = $this -> conn -> prepare($sql);
        $statement -> execute(array($username, $password, $fullname, $birthdate, $email, 'user'));
    }

    // Obtiene las aulas por nombre (descripción) o nombre corto
    function getAulas($shortname, $name, $date){
        if ($shortname != null && $name != null)
        {
            $sql = "select * from " . TABLE_AULA . " where shortname = ? and name = ?";
            $statement = $this -> conn -> prepare($sql);
            $statement -> execute(array($shortname, $name));
            return $statement;
        }
        else if ($shortname != null){
            $sql = "select * from " . TABLE_AULA . " where shortname = ?";
            $statement = $this -> conn -> prepare($sql);
            $statement -> execute(array($shortname));
            return $statement;
        }
        else if ($name != null){
            $sql = "select * from " . TABLE_AULA . " where name = ?";
            $statement = $this -> conn -> prepare($sql);
            $statement -> execute(array($name));
            return $statement;
        }
        else if ($shortname == null && $name == null){
            $sql = "select * from " . TABLE_AULA;
            return $this -> conn -> query($sql);
        }

    }

    // Obtengo los tramos y si para este aula y fecha están reservados o no
    function getTramosAndReservedOnes($shortnameaula, $date){
        // if exists(select tramo from reserva where fecha = $date and aula = $shortnameaula and tramo = (select numtramo from tramo where tramo =  ''   ) )    // -> es que está reservado
        // if count(columns) == 0

        $sql = "select * from " . TABLE_TRAMO . " order by numtramo asc";
        $statement = $this -> conn -> query($sql);

        $columns = $statement -> fetchAll();
        $tramos = array();
        for($i = 0; $i < count($columns); $i++){
            $tramos[$i]["tramo"] = $columns[$i]["tramo"];
            $estetramo = $columns[$i]["tramo"];

            // Compruebo si el tramo esta reservado (si el count devuelto es > 0 es que está reservado):
            $sqlReserved =  "select count(*) from tramo where exists (select tramo from reserva where fecha = '$date' and aula = '$shortnameaula' and tramo = (select numtramo from tramo where tramo = '$estetramo' ) ) ";
            // echo "<br/><br/>" .  $sqlReserved;
            $columnsReserved = $this -> conn -> query($sqlReserved) -> fetchAll();
            // echo "<br/><br/>" . $columnsReserved[0]["count(*)"];
            if ($columnsReserved[0]["count(*)"] > 0)
                $tramos[$i]["reservado"] = "si";
            else
                $tramos[$i]["reservado"] = "no";

        }

        // print_r($tramos);
        return $tramos;
    }




    function getTramosUserAndReservedOnes($shortnameaula, $date){
        // if exists(select tramo from reserva where fecha = $date and aula = $shortnameaula and tramo = (select numtramo from tramo where tramo =  ''   ) )    // -> es que está reservado
        // if count(columns) == 0

        $sql = "select * from " . TABLE_TRAMO . " order by numtramo asc";
        $statement = $this -> conn -> query($sql);

        $columns = $statement -> fetchAll();
        $tramos = array();
        for($i = 0; $i < count($columns); $i++){
            $tramos[$i]["tramo"] = $columns[$i]["tramo"];
            $estetramo = $columns[$i]["tramo"];

            // Compruebo si el tramo esta reservado (si el count devuelto es > 0 es que está reservado):
            $sqlReserved =  "select count(*) from tramo where exists (select tramo from reserva where fecha = '$date' and aula = '$shortnameaula' and tramo = (select numtramo from tramo where tramo = '$estetramo' ) ) ";
            // echo "<br/><br/>" .  $sqlReserved;
            $columnsReserved = $this -> conn -> query($sqlReserved) -> fetchAll();
            // echo "<br/><br/>" . $columnsReserved[0]["count(*)"];
            if ($columnsReserved[0]["count(*)"] > 0){
                $tramos[$i]["reservado"] = "si";

                $sqlobtainuser = "select username from reserva where fecha = '$date' and aula = '$shortnameaula' and tramo = (select numtramo from tramo where tramo = '$estetramo')";
                $columnsUser = $this -> conn -> query($sqlobtainuser) -> fetchAll();
                $usuario = $columnsUser[0]["username"];
                $tramos[$i]["usuario"] = $usuario; // select user from reserva where fecha = $date and aula = $shortnameaula and tramo = $estetramo
            }
            else
                $tramos[$i]["reservado"] = "no";

        }

        // print_r($tramos);
        return $tramos;
    }

    function realizarReserva($username, $shortnameaula, $date, $tramo, $descripcionUso){
        $sql = "INSERT INTO " . TABLE_RESERVA . " VALUES(?, ?, ?, ?, ?)";
        $statement = $this -> conn -> prepare($sql);

        $sqlTramo = "select numtramo from " . TABLE_TRAMO . " where tramo = '" . $tramo . "'";
        $columntramo = $this -> conn -> query($sqlTramo) -> fetchAll();
        $tramoId = $columntramo[0]["numtramo"];
        // echo $tramoId;

        $statement -> execute(array($username, $shortnameaula, $date, $tramoId, $descripcionUso));
    }

    // Obtengo todas las reservas cuya fecha no ha pasado
    function getAllReserves(){
        $sql = "select fecha, (select tramo from tramo where reserva.tramo = numtramo) as tramo, aula, descripcionUso as 'descipcion de uso', username as usuario from " . TABLE_RESERVA . " where fecha >= date(now()) order by fecha asc";
        return $this -> conn -> query($sql);
    }

    // Anulo reserva
    function anularReserva($aula, $fecha, $tramo){
        $sql = "delete from " . TABLE_RESERVA . " where aula = '$aula' and fecha = '$fecha' and tramo = (select numtramo from tramo where tramo = '$tramo')";
        return $this -> conn -> exec($sql);
    }


    function getUsosAula(){
        $sql = "SELECT * from " . TABLE_USOSAULA;
        return $this -> conn -> query($sql);
    }


    // Devuelve el nuevo ID a insertar (el máximo que esté insertado + 1)
    function selectNewIdReservasAnuladas(){
        $sql = "SELECT max(" . COLUMN_ID_RESERVADAS . ") FROM " . TABLE_RESERVASANULADAS;
        $statementSelect = $this -> conn -> query($sql);
        $columnsSelect =  $statementSelect -> fetchAll();
        $max = $columnsSelect[0]["max(id)"];

        if (empty($max)) // si es null
            $id = 1;
        else
            $id = $max + 1;

        return $id;
    }
    
    function insertReservasAnuladas($aula, $user, $description){        
        $sqlInsert = "INSERT INTO " . TABLE_RESERVASANULADAS . " VALUES (?, ?, ?, ?)";
        $statement = $this -> conn -> prepare($sqlInsert);
        $id = $this -> selectNewIdReservasAnuladas();
        $statement -> execute(array($id, $aula, $user, $description));

    }


    // Compruebo si es admin (si en la tabla user su categoría es 'admin')
    function isTheAdmin($user){
        $sql = "SELECT COUNT(*) FROM " . TABLE_USER . " WHERE " . COLUMN_USERNAME . " = '" . $user . "' AND " . COLUMN_USER_CATEGORY . " = 'ADMIN'";
        $statement = $this -> conn -> query($sql);
        $columns = $statement -> fetchAll();
        if ($columns[0]["COUNT(*)"] > 0)
            return true;
        return false;
    }

    function getReservasAnuladas(){
        $sql = "SELECT * FROM " . TABLE_RESERVASANULADAS;
        return $this -> conn -> query($sql);
    }

    
}


?>