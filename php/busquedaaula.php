<?php
include_once("App.php");
App::print_head("Búsqueda de aulas para reservar");

$app = new App();
$app -> validateSession();

if ($app -> isTheAdmin())
    App::print_nav2();
else
    App::print_nav1();
?>

<br/>
<div class="row h-100 justify-content-center align-items-center">

<form method='POST' action='./listaAulas.php'>

    <label><br/>Nombre corto del aula: </label>
    <input name="shortname" type="text" class="form-control" />

    <label><br/>Descripcion: </label>
    <input name="name" type="text" class="form-control" />

    <br/><br/><label>Fecha:</label>
    <input class="form-control" type='date' name='date' required > 

    <!--Botón buscar -->
    <br/><br/><input class="bg-primary form-control text-white" type='submit' value='Consultar aulas'/>


</form>

</div>





<?php
include_once("App.php");
App::print_footer();

?>