<?php
include_once("dao.php");

class App{

    private $dao;

    public function saveSession($username){
        session_start();
        $_SESSION["username"] = $username;
    }

    public function validateSession(){
        session_start();
        if (!isset($_SESSION["username"])){
            echo "<script language='javascript'>window.location.href='./login.php'</script>";
        }
    }

    public function invalidateSession(){
        session_start();
        if (isset($_SESSION["username"]))
            unset($_SESSION["username"]);
        session_destroy();
        echo "<script language='javascript'>window.location.href='./login.php'</script>";
    }

    public function __construct(){
        $this -> dao = new dao();
    }

    function validateUser($username, $passowrd){
        return $this -> dao -> validateUser($username, $passowrd);
    }

    function getIfUserExists($username){
        return $this -> dao -> getIfUserExists($username);
    }

    function getIfEmailExists($email){
        return $this -> dao -> getIfEmailExists($email);
    }

    function registerUser($username, $password, $fullname, $birthdate, $email){
        $this -> dao -> registerUser($username, $password, $fullname, $birthdate, $email);
    }

    function getAulas($shortname, $name, $date){
        return $this -> dao -> getAulas($shortname, $name, $date);
    }

    function getTramosAndReservedOnes($shortnameaula, $date){
        return $this -> dao -> getTramosAndReservedOnes($shortnameaula, $date);
    }

    function realizarReserva($username, $shortnameaula, $date, $tramo, $descripcionUso){
        $this -> dao -> realizarReserva($username, $shortnameaula, $date, $tramo, $descripcionUso);
    }
    
    function getAllReserves(){
        return $this -> dao -> getAllReserves();
    }

    function anularReserva($aula, $fecha, $tramo){
        return $this -> dao -> anularReserva($aula, $fecha, $tramo);
    }

    function getTramosUserAndReservedOnes($shortnameaula, $date){
        return $this -> dao -> getTramosUserAndReservedOnes($shortnameaula, $date);
    }

    function getUsosAula(){
        return $this -> dao -> getUsosAula();
    }

    function insertReservasAnuladas($aula, $user, $description){    
        $this -> dao -> insertReservasAnuladas($aula, $user, $description);    
    }

    function isTheAdmin(){
        return $this -> dao -> isTheAdmin($_SESSION["username"]);
    }

    
    function getReservasAnuladas(){
        return $this -> dao -> getReservasAnuladas();
    }



public static function print_head($title){
    echo "
    <!DOCTYPE html>
        <html>
        <head>
        <meta http-equiv='Content-type' content='text/html; charset=utf-8' />

            <title>$title</title>
            <link rel='stylesheet' type='text/css' href='./../css/bootstrap.css'/>
            <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>
            <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>
            
        </head>

        <body>
            <div class='text-center bg-primary text-white'>
               <br/> <h1>$title</h1>  <br/> 
            </div>  
        </body>
    ";
}

public static function print_footer(){
    echo "<footer class='fixed-bottom'>
    <div class='text-center bg-primary text-white'>
        <br/><h4>Marina Espinosa Gálvez </h4>
        <a class='text-white' href=\"http://www.linkedin.com/in/marina-espinosa-galvez\">LinkedIn</a><br/>
        <h4>GitHub</h4>
        <a class='text-white' href=\"https://github.com/NanakoKasane\">Mi GitHub</a><br/>
        </div>
    </div>

    </footer>
    </html>";
}

public static function print_nav1(){
    echo'<nav class="navbar-dark bg-primary navbar navbar-expand-lg navbar-expand-lg center" >


    <a class="navbar-brand" href="./busquedaaula.php">Búsqueda de aula</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        
      <a class="navbar-brand" href="./gestionreservas.php">Listado de reservas</a>';
  
      echo '<a class="nav-item nav-link active" href="./logout.php">Logout <span class="sr-only">(current)</span></a>
  
      </div>
    </div>
  </nav>';
}

public static function print_nav2(){
    echo'<nav class="navbar-dark bg-primary navbar navbar-expand-lg navbar-expand-lg center" >


    <a class="navbar-brand" href="./busquedaaula.php">Búsqueda de aula</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        
      <a class="navbar-brand" href="./gestionreservas.php">Listado de reservas</a>
      <a class="navbar-brand" href="./consultacancelaciones.php">Consulta de cancelaciones</a>

      <a class="nav-item nav-link active" href="./logout.php">Logout <span class="sr-only">(current)</span></a>
  
      </div>
    </div>
  </nav>';
}






}



?>