<?php
include_once("App.php");
header('Content-type: text/html; charset=utf-8');

if ($_GET){
    if (isset($_GET["shortname"]))
        $shortname = $_GET["shortname"];
    if (isset($_GET["date"]))
        $date = $_GET["date"];

    App::print_head("Reserva del aula \"$shortname\" el día $date");

    $app = new App();
    $app -> validateSession();

    if ($app -> isTheAdmin())
        App::print_nav2();
    else
        App::print_nav1();

    $tramos = $app -> getTramosAndReservedOnes($shortname, $date);
    echo "<table class='table'>";
        
    echo "<tr>";
        echo "<th scope='col'><div>" . strtoupper("tramo") . "</div></th>";
        echo "<th scope='col'><div>" . strtoupper("reservado") . "</div></th>";
    echo "<th></th>";
    echo "</tr>";

        for($i = 0; $i < count($tramos); $i++){
            echo "<tr scope='row'>";
            echo "<td>" . $tramos[$i]["tramo"] . "</td>";
            echo "<td>" . strtoupper($tramos[$i]["reservado"]) . "</td>";

            $reservado = $tramos[$i]["reservado"];
            echo "<td>";
            if ($reservado != "si"){
                $tramo = $tramos[$i]["tramo"];
                echo "<form METHOD='GET' action='altareserva.php'>".
                "<input class='btn-primary btn text-white' type='submit' value='Alta reserva'/>" .
                "<input type='hidden' value='$date' name='date'/><input type='hidden' value='$tramo' name='tramo'/><input type='hidden' value='$shortname' name='shortname'</input> </form>";
            }
            echo "</td>";

            echo "</tr>";
        }

    echo "</table>";
}
else{
    App::print_head("Reserva de aula");
    $app = new App();
    if ($app -> isTheAdmin())
        App::print_nav2();
    else
        App::print_nav1();

    echo "<br/><h5 class='text-center'> Debe elegir un aula a reservar y una fecha antes de poder reservar </h5>";
}


?>



<?php
include_once("App.php");
App::print_footer();
?>