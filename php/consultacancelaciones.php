<?php
include_once("App.php");
header('Content-type: text/html; charset=utf-8');
App::print_head("Consulta de cancelaciones");

$app = new App();
$app -> validateSession();

if ($app -> isTheAdmin())
    App::print_nav2();
else
    App::print_nav1();


    // Lista
    $statement = $app -> getReservasAnuladas();
    if ($statement -> rowCount() == 0){
        echo "<script language='javascript'>window.alert('No hay cancelaciones');</script>";
    }
    else{
        echo "<table class='table'>";
        echo "<tr>";
        for($i = 0; $i < $statement -> columnCount(); $i++){
            echo "<th>" . strtoupper($statement -> getColumnMeta($i)["name"]) . "</th>";
        }
        echo "</tr>";

        $columns = $statement -> fetchAll();
        for($i = 0; $i < count($columns); $i++){
            echo "<tr>";
            echo "<td>" . $columns[$i]["id"] . "</td>";
            echo "<td>" . $columns[$i]["aula"] . "</td>";
            echo "<td>" . $columns[$i]["user"] . "</td>";
            echo "<td>" . $columns[$i]["description"] . "</td>";

            echo "</tr>";
        }   
        echo "</table>";

    }












    App::print_footer();
?>