<?php
include_once("App.php");
header('Content-type: text/html; charset=utf-8');

$app = new App();
$app -> validateSession();

if ($_GET){
    if (isset($_GET["shortname"]))
        $shortname = $_GET["shortname"];
    if (isset($_GET["date"]))
        $date = $_GET["date"];
    if (isset($_GET["tramo"]))
        $tramo = $_GET["tramo"];
        
        App::print_head("Alta de la reserva del aula \"$shortname\" el día $date en el tramo horario $tramo");

    echo '
    <div class="row h-100 justify-content-center align-items-center">
        <form method=POST action="./altareserva.php?date=' . $date . '&tramo=' . $tramo . '&shortname=' . $shortname .'">
            <br/>
            <label>Describa el uso que se le dará al aula</label> <br/>
            <select name="motivo" class="form-control"> <option  disabled selected value>-- Seleccione un motivo --</option>';
            $resultset = $app -> getUsosAula();
            $columnas = $resultset -> fetchAll();
                for($i = 0; $i < count($columnas); $i++){
                    $uso = $columnas[$i]["uso"] ;
                    echo "<option value='$uso'>" . $uso . "</option>";
                }
            echo '</select><br/>
            <textarea maxlength="70" class="form-control" name="descripcionUso" placeholder="Descripción del uso"></textarea> 
            <br/>
            <input class="btn-primary btn text-white form-control" type="submit" value="Finalizar reserva"/>
        </form>
    </div>
    ';
}
else{
    App::print_head("Alta de reserva");
    $app = new App();
    if ($app -> isTheAdmin())
        App::print_nav2();
    else
        App::print_nav1();
    
    echo "<br/><h5 class='text-center'> Debe elegir un aula a reservar, una fecha y un tramo horario antes de poder dar de alta una reserva </h5>";

}



if ($_POST){
    // Recojo el usuario que inició sesión
    if (!isset($_SESSION))
        session_start();
    $username = $_SESSION["username"];
    // echo $username;
    $app = new App();

    $descripcionUso = null;
    $motivo = null;
    if (isset($_POST["descripcionUso"]))
        $descripcionUso = $_POST["descripcionUso"];

    if (isset($_POST["motivo"])){
        $motivo = $_POST["motivo"];
    }

    if (empty($motivo) && empty($descripcionUso))
        echo "<script language='javascript'>window.alert('Debe introducir un motivo de uso del aula') </script>";
    else{
        $descripcion = $descripcionUso;
        if (!empty($motivo) && empty($descripcionUso))
            $descripcion = $motivo;
        else if (!empty($motivo) && !empty($descripcionUso))
            $descripcion = $motivo . " (" . $descripcionUso . ")";

        // Insertar en la BBDD
        $app -> realizarReserva($username, $shortname, $date, $tramo, $descripcion);
        echo "<script language='javascript'>window.alert('Se ha realizado su reserva con éxito') </script>";
        echo "<script language='javascript'>window.location.href='./gestionreservas.php' </script>";

        // IR A -> gestionreservas.php -> que muestra todas las reservas (similar a reservaaula pero con el nombre de quien reserva si está reservada y si eres tu, poder anular reserva)
    }

}


App::print_footer();

?>