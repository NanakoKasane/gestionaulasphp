<?php
include_once("App.php");
App::print_head("Registro");
?>

<div class="container" >
    <div class="row">
        <div class="col-12 col-md-4 offset-md-4">
            <form method="POST" action="./register.php">
                <br/><label>Nombre de usuario: </label> <input class="form-control" type="text" required name="username"/><br/>
                <label>Contraseña: </label> <input class="form-control" type="password" required name="password"/><br/>
                <label>Nombre y apellidos: </label> <input class="form-control" type="text" required name="fullname"/> <br/>
                <label>Fecha de nacimiento: </label> <input class="form-control" type="date" required name="birthdate"/> <br/>
                <label>Email: </label> <input class="form-control" type="text" required name="email"/> <br/><br/>
                <input class="bg-primary form-control text-white" type="submit" value="Registrar"/><br/><br/>
                <a class="text-center text-white bg-primary form-control" href='./login.php' >Volver al Inicio de sesión</a>

            </form>
        </div>
    </div>
</div>




<?php
include_once("App.php");
App::print_footer();

$app = new App();

if ($_POST){
    if (isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["fullname"]) && isset($_POST["birthdate"]) && isset($_POST["email"])){
        $username = $_POST["username"];
        $password = $_POST["password"];
        $fullname = $_POST["fullname"];
        $birthdate = $_POST["birthdate"];
        $email = $_POST["email"];

        // Compruebo que sea mayor de edad:
        $dateValid = true;
        //if((time()-(60*60*24)) < strtotime($birthdate)){

        if (!mayorDeEdad($birthdate)){
            echo "<script language='javascript'>window.alert('Es necesario ser mayor de edad para registrarse'); </script>";
            $dateValid = false;
        }
        // }
        
        // Debo comprobar que username y email no existan ya en la BBDD
        $userExists = $app -> getIfUserExists($username);
        $emailExists = $app -> getIfEmailExists($email);

        if ($userExists)
            echo "<script language='javascript'>window.alert('El usuario ya existe. Debe elegir un nombre de usuario único') </script>";
        if ($emailExists)
            echo "<script language='javascript'>window.alert('El email ya existe. Debe elegir un email único') </script>";

        if (!$userExists && !$emailExists && $dateValid){
            // Inserto el usuario:
            $app -> registerUser($username, $password, $fullname, $birthdate, $email);
            echo "<script language='javascript'>window.alert('Registrado usuario con éxito') </script>";
            echo "<script language='javascript'>window.location.href='./login.php'</script>";
        }

    }
    else{
        // Deben introducirse todos los datos
    }

}

function mayorDeEdad($fecha)
    {
        $mayor=18;

        $nacio = DateTime::createFromFormat('Y-m-d', $fecha);
        $calculo = $nacio->diff(new DateTime()); // Diferencia entre fecha y fecha actual
        $edad=  $calculo->y;    

        if ($edad < $mayor) {
            
            return false;  // No es mayor de edad
        }
        else{
            return true;  // Es mayor de edad
        }
    }

?>