<?php
include_once("App.php");
header('Content-type: text/html; charset=utf-8');

$app = new App();
$app -> validateSession();

if ($_GET){
    if (isset($_GET["name"]) && isset($_GET["date"])){
        $name = $_GET["name"];
        $date = $_GET["date"];
        App::print_head("Consulta de reservas del aula '$name'");     
        if ($app -> isTheAdmin())
            App::print_nav2();
        else
            App::print_nav1();

        // $tramos = $app -> getTramosUserAndReservedOnes($name, date("Y-m-d")); // Si no se especifica fecha, se sobreentiende que es hoy
        // mostrarReservas($tramos, $date);
    }
    else if (isset($_GET["shortname"]) && isset($_GET["date"])){
        $shortname = $_GET["shortname"];
        $date = $_GET["date"];   
        App::print_head("Consulta de reservas del aula '$shortname'");     
        if ($app -> isTheAdmin())
            App::print_nav2();
        else
            App::print_nav1();
        $tramos = $app -> getTramosUserAndReservedOnes($shortname,  $date); // Si no se especifica fecha, se sobreentiende que es hoy
        mostrarReservas($tramos, $date, $shortname);
    }


}


// funcion mostrar datos($resultset)

function mostrarReservas($tramos, $date, $shortname){
    echo "<table class='table'>";
echo "<tr>";
    echo "<th scope='col'><div>" . strtoupper("tramo") . "</div></th>";
    echo "<th scope='col'><div>" . strtoupper("reservado") . "</div></th>";

echo "<th></th>";
echo "</tr>";

    for($i = 0; $i < count($tramos); $i++){
        echo "<tr scope='row'>";
        echo "<td>" . $tramos[$i]["tramo"] . "</td>";
        echo "<td>" . strtoupper($tramos[$i]["reservado"]) . "</td>";

        $reservado = $tramos[$i]["reservado"];


        if ($reservado != "si"){     
            echo "<td>";
            $tramo = $tramos[$i]["tramo"];
            echo "<form METHOD='GET' action='altareserva.php'>".
            "<input class='btn-primary btn text-white' type='submit' value='Alta reserva'/>" .
            "<input type='hidden' value='$date' name='date'/><input type='hidden' value='$tramo' name='tramo'/><input type='hidden' value='$shortname' name='shortname'</input> </form>";
            echo "</td>";
        }
        else{
            // MOSTRAR USUARIO:
            echo "<td> (Reservada por: " . $tramos[$i]["usuario"] . " )</td>";
        }

        echo "</tr>";
    }

echo "</table>";
}

?>



<?php
include_once("App.php");
App::print_footer();
?>