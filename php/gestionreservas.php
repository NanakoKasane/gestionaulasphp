<?php
include_once("App.php");
header('Content-type: text/html; charset=utf-8');
App::print_head("Gestión de reservas");
$app = new App();

$app -> validateSession();
if ($app -> isTheAdmin())
    App::print_nav2();
else
    App::print_nav1();


// TODAS LAS RESERVAS CON FECHA > HOY
// Si es tuya -> muestra también opción de anular con una explicación.
// Si no es tuya, muestro el user que reservó

$statement = $app -> getAllReserves();

echo "<table class='table'>";

echo "<tr scope='row'>";
    for($i = 0; $i < $statement -> columnCount(); $i++){
        echo "<th scope='col'>" .  strtoupper($statement -> getColumnMeta($i)["name"]) . "</th>";
    }
echo "<th></th>";
echo "<th></th>";
echo "</tr>";
$columns = $statement -> fetchAll();

for($i = 0; $i < count($columns); $i++){
    echo "<tr scope='row'>";

        $descripcionuso =  $columns[$i]["descipcion de uso"] ;      
        $usuario = $columns[$i]["usuario"] ;
        $fecha = $columns[$i]["fecha"];
        $tramo = $columns[$i]["tramo"];
        $aula = $columns[$i]["aula"] ;

        echo "<td>". $fecha . "</td>";
        echo "<td>". $tramo . "</td>";
        echo "<td>". $aula . "</td>";
        echo "<td>". $descripcionuso . "</td>";
        echo "<td>". $usuario. "</td>";
        
        if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
        $usuarioActual = $_SESSION["username"];
        if ($usuario == $usuarioActual){
            echo "<td>" .  "<form METHOD='POST' action='./gestionreservas.php'>".
            "<textarea maxlength='70' name='motivo' placeholder='Introduzca el motivo de la cancelación...' required class='form-control' ></textarea></td>"  .
            "<td> <input class='btn-primary btn text-white' type='submit' value='Anular reserva'/>" . "</td> <input type='hidden' name='descripcionuso' value='" .$descripcionuso . "'/>" .
            "<input type='hidden' name='fecha' value='" .$fecha . "'/>" . "<input type='hidden' name='tramo' value='" .$tramo . "'/>" . "<input type='hidden' name='aula' value='" .$aula . "'/>";
            echo "</form>";
        }
        else {
            echo "<td>" . "" . "</td>" . "<td>" . "" . "</td>";
        }


    
    echo "</tr>";



}

?>


<?php
include_once("App.php");

if ($_POST){
    if (isset($_POST["motivo"]) && isset($_POST["descripcionuso"])){
        // comprobar que sea diferente a desctipcion uso
        $motivo = $_POST["motivo"];
        $descripcionuso = $_POST["descripcionuso"];

        if ($motivo == $descripcionuso)
            echo "<script language='javascript'>window.alert('El motivo de cancelación debe ser diferente al de uso')</script>";
        else{
            $tramo = $_POST["tramo"];
            $fecha = $_POST["fecha"];
            $aula = $_POST["aula"];

            // Inserto en reservas anuladas:
            $app = new App();

            $app -> insertReservasAnuladas($aula, $usuarioActual, $motivo);

            // Anulo la reserva
            if ($app -> anularReserva($aula, $fecha, $tramo) > 0) // Si se ha borrado correctamente
            {
                $cargadoYa = null;
                echo "<script language='javascript'>window.alert('Se ha cancelado la reserva del aula con éxito');</script>";
                if (empty($cargadoYa)){
                    echo "<script language='javascript'>window.location.reload();</script>";
                    $cargadoYa = true;
                }
            }

        }
    }
}

App::print_footer();
?>