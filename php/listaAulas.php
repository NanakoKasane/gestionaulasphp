<?php
include_once("App.php");
header('Content-type: text/html; charset=utf-8');

$app = new App();
$app -> validateSession();

if ($_GET){
}


if ($_POST){
    // echo "POST";

    // Valido los datos
    $shortname = null;
    $name = null;
    $date = null;

    if (isset($_POST["shortname"])){
        $shortname = $_POST["shortname"];
    }
    if (isset($_POST["name"])){
        $name = $_POST["name"];
    }
    if (isset($_POST["date"])){
        $date = $_POST["date"];   
         if((time()-(60*60*24)) >= strtotime($date)){
            echo "<script language='javascript'>window.alert('Debe introducir una fecha de reserva igual o posterior a hoy')</script>";
            echo "<script language='javascript'>window.location.href='./busquedaaula.php'</script>";
        }
        else{
            // Consulta de obtener las aulas:
            App::print_head("Listado de aulas para reservar el día $date");

            $app = new App();
            if ($app -> isTheAdmin())
                App::print_nav2();
            else
                App::print_nav1();

            $resultset = $app -> getAulas($shortname, $name, $date);
            showAulas($resultset, $date);

        }
    }


  //  if ($date == null && $name == null){
 //       echo "<script language='javascript'>window.alert('Debe introducir al menos un campo para buscar')</script>";
  //      echo "<script language='javascript'>window.location.href='./busquedaaula.php'</script>";
 //   }



 // La fecha es la fecha a reservar luego


        // IMPORTANTE:
        // Si haces click en un aula te permite reservarla y consultar sus reservas


        
}

else{
    App::print_head("Listado de aulas");
    App::print_nav1();
    echo "<br/><h5 class='text-center'> Debe realizar una búsqueda de aulas por fecha para poder ver el listado de aulas a reservar </h5>";
}



function showAulas($resultset, $date){
    $columns = $resultset -> fetchAll();
    if (count($columns) == 0){
        echo "<script language='javascript'>window.alert('No hay ningún aula con los datos introducidos')</script>";
        echo "<script language='javascript'>window.location.href='./busquedaaula.php'</script>";
        return;
    }

    //echo '<div id="scrolltable">';
    echo "<table class='table'>";
    
    echo "<tr>";
    for($i = 0; $i < $resultset -> columnCount(); $i++){
        echo "<th scope='col'><div>" . strtoupper($resultset -> getColumnMeta($i)["name"]) . "</div></th>";
    }
    echo "<th></th>";
    echo "</tr>";

    $shortname = null;
    for($i = 0; $i < count($columns); $i++){
        echo "<tr scope='row'>";

        echo "<td>";
            $shortname = $columns[$i]["shortname"];
            echo "<a href='./consultarreservas.php?shortname=$shortname&date=$date' TITLE='Consulta las reservas del aula \"$shortname\"'>" . $shortname . "</a>";    
        echo "</td>";

        echo "<td>";
            $name = $columns[$i]["name"];
            // echo "<a href='./consultarreservas.php?name=$name&date=$date' TITLE='Consulta las reservas del aula \"$name\"'>" . $name . "</a>";   
            echo $name;
        echo "</td>";

        echo "<td>";
            echo $columns[$i]["location"];
        echo "</td>";

        echo "<td>";
            $tic = null;
            if($columns[$i]["TIC"])
                $tic = "SÍ";
            else
                $tic = "NO";
            echo $tic;
        echo "</td>";

        echo "<td>";
            echo $columns[$i]["numberPC"];
        echo "</td>";

        echo "<td>";
        echo '
        <script>
        function irAReserva(shortname) {
        window.location.href="./reservaaula.php?id=shortname";
        }
        </script>
        ';
        echo "<form METHOD='GET' action='reservaaula.php'>".
        "<input class='btn-primary btn text-white' type='submit' value='Reservar aula'/>" .
        "<input type='hidden' value='$date' name='date'/><input type='hidden' value='$shortname' name='shortname'</input> </form>";
        // echo "<button type=\"button\" class=\"btn btn-primary\"  onclick=\"irAReserva($shortname)\">Reservar aula</button>";
        echo "</td>";


        echo "</tr>";
    }

    echo "</table>";
    // echo '</div>';

}



?>





<?php
include_once("App.php");
App::print_footer();
?>